# ymc-odoo

#### **1.1. Pre-requisites**
Ensure you have Docker desktop installed.
You will also need to have Python installed and a Python virtual environment to run the Fabric3 tasks.
Step-by-step setup instructions below are for Mac OS in Terminal to set up Python, Python virtual environment and Fabric3 script dependencies.
Clone the project repository and checkout staging repo:
```
git clone git@gitlab.com:allsolve/ymc-odoo.git
cd ymc-odoo
git checkout -b development
```

Install Homebrew SKIP IF EXISTED ALREADY
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Install Python 3.7 SKIP IF EXISTED ALREADY
```
brew install https://raw.githubusercontent.com/Homebrew/homebrew-core/8ac15a745713a8973fce6656cb0f7e8710230734/Formula/python.rb
```

Create Python virtual environment
```
python3 -m venv env
```

Activate the virtual environment (remember to do this everytime you work with this project, for each terminal window)
```
source env/bin/activate
```

Upgrade PIP (Python package manager)
```
pip install --upgrade pip
```

Install Fabric3 and dependencies
```
pip install Fabric3==1.14.post1
```

Make a copy of the local environment variable script:
```
cp dev_env.sh-sample dev_env.sh
```

Edit dev_env.sh and fill up with the project secrets/credentials (can be obtained from Gitlab CI/CD variables, or ask the team lead for this)

Set up environment variables
```
source dev_env.sh
```

Pull a copy of development database into the local development database
```
fab staging pull_db_and_update_local_dev
```

#### **1.2. Starting up development environment**
Step-by-step setup instructions below are for Mac OS in Terminal. These commands must be executed in the project root folder:
Activate the virtual environment (if not already activated):
```
source env/bin/activate
```

Set up environment variables
```
source dev_env.sh
```

Bring up the docker services:
```
docker-compose -f docker-compose-dev.yml up -d
```

The docker-compose script will build and run Odoo and PostgreSQL services locally for this project.
You can then access the local development server via: http://localhost

#### **1.3. Shutting down development environment**
NOTE: must be executed in the project root folder
```
docker-compose -f docker-compose-dev.yml down
```

#### **1.4. View running docker services**
```
docker ps
```

#### **1.5. Deploying code changes to local** development server
NOTE: must be executed in the project root folder
```
fab localdev deploy
```

#### **1.6. Permission Error (if exists)**
Most likely the docker copy file is not executable. If this happens, run chmod +x in the docker shell

i.e init-and-update-db could not run due to permission error.

```
chmod +x init-and-update-db.py
```

If shell is inaccessible, make sure that the file copy is executable before running fab staging command
