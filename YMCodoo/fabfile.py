"""CI/CD automation script
This script contains the automation tasks for setting up a new server and
continuous deployment task for the YMC Odoo project. The script primarily
used by Gitlab CI/CD for build/deploy automation for each code commit to
development/master branch.

Setup task has to be manually invoked, typically:
fab staging setup

Take note that this script requires at minimum these environment variables
set up in the shell before invocation of the 'setup' task:

PRODUCTION_SSH_HOST and PRODUCTION_SSH_KEY for production deploy
STAGING_SSH_HOST and STAGING_SSH_KEY for staging deploy


This setup script has been tested on:
Ubuntu 16.04 (Xenial), Ubuntu 18.04 (Bionic) and Debian 9.5 (Stretch)
"""

import os
from fabric.api import env, require, sudo, put, local, get
from fabric.context_managers import cd, settings, shell_env, quiet
from fabric.contrib import files


# Prefix for all application environment variable key name to be passed to
# remote server
APP_ENV_VAR_PREFIX = 'APP_'

# Some Gitlab-CI environment variables that we will be using
CI_REGISTRY = os.getenv('CI_REGISTRY')
CI_REGISTRY_USER = os.getenv('CI_REGISTRY_USER')
CI_REGISTRY_IMAGE = os.getenv('CI_REGISTRY_IMAGE')
CI_COMMIT_REF_NAME = os.getenv('CI_COMMIT_REF_NAME')
CI_JOB_TOKEN = os.getenv('CI_JOB_TOKEN')

# Application specific variables
env.project = 'ymc-odoo'
env.app_dir = f'apps/{env.project}'
env.deploy_env = 'staging'


def _helper_get_gitlab_ci_env_vars():
    """
    Function extracts only the necessary Gitlab CI environment variables to
    be passed to the deploy environment to be used in the docker-compose script
    running on the deployed environment.
    Also extracts any keys with prefix in APP_ENV_VAR_PREFIX
    """
    ci_env_keys = [
        'CI_REGISTRY_IMAGE', 'CI_COMMIT_REF_NAME',
    ]

    app_keys = [
        key for key in list(os.environ.keys())
        if key.startswith(APP_ENV_VAR_PREFIX)
    ]

    env_keys = ci_env_keys + app_keys

    return {key: os.getenv(key) for key in env_keys}


# Gitlab CI environment variables to pass to remote server
env.ci_env_vars = _helper_get_gitlab_ci_env_vars()


# Remote environments
def production():
    # Server credentials
    env.hosts = [os.getenv('PRODUCTION_SSH_HOST')]
    env.key = os.getenv('PRODUCTION_SSH_KEY')
    env.sudo_password = os.getenv('PRODUCTION_SUDO_PASS')

    env.deploy_env = 'production'


def staging():
    # Server credentials
    env.hosts = [os.getenv('STAGING_SSH_HOST')]
    env.key = os.getenv('STAGING_SSH_KEY')
    env.sudo_password = os.getenv('STAGING_SUDO_PASS')

    env.deploy_env = 'staging'


def development():
    # Server credentials
    env.hosts = [os.getenv('DEVELOPMENT_SSH_HOST')]
    env.key = os.getenv('DEVELOPMENT_SSH_KEY')
    env.sudo_password = os.getenv('DEVELOPMENT_SUDO_PASS')

    env.deploy_env = 'development'


def localdev():
    env.deploy_env = 'localdev'


# Helper tasks
def lsb_release():
    sudo('lsb_release -a')


def update_files():
    """
    Copies the docker-compose.yml script and other files to the remote server
    """
    # Typically docker-compose script and other dependencies
    put('docker-compose.yml', env.app_dir, use_sudo=True)
    sudo(f"mkdir -p {os.path.join(env.app_dir, 'docker/nginx')}")
    if (not os.getenv('APP_SSL_DOMAIN', False) or not
            os.getenv('APP_SSL_ADMIN_EMAIL', False)):
        put(
            'docker/nginx/nginx-nocert.conf',
            os.path.join(env.app_dir, 'docker/nginx/nginx.conf'),
            use_sudo=True,
        )
    else:
        put(
            'docker/nginx/nginx.conf',
            os.path.join(env.app_dir, 'docker/nginx/nginx.conf'),
            use_sudo=True,
        )


def upgrade():
    sudo('apt-get update')
    sudo('DEBIAN_FRONTEND=noninteractive apt-get upgrade -y')
    # Workaround to avoid Docker login from failing on ubuntu 18.04
    # Ref: https://stackoverflow.com/questions/50151833/cannot-login-to-docker-
    # account
    sudo('DEBIAN_FRONTEND=noninteractive apt-get install -y gnupg2 pass')


def setup_swap():
    """Setup a 2GB swap space on the remote server, if no swap space exists"""
    result = sudo('swapon --show', warn_only=True)
    swap_exists = len(result) > 0

    if swap_exists:
        local('echo Swap exists, skipping...')
        return

    sudo('fallocate -l 2G /swapfile')
    sudo('chmod 600 /swapfile')
    sudo('mkswap /swapfile')
    sudo('swapon /swapfile')
    sudo('cp /etc/fstab /etc/fstab.bak')
    sudo("echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab")


def setup_ufw():
    """
    Setup a UFW firewall on the remote server with ports 22, 80 and 443 open
    by default
    """
    result = sudo('which ufw', warn_only=True)
    if len(result) == 0:
        sudo('apt-get install -y ufw')

    sudo('ufw default deny incoming')
    sudo('ufw default allow outgoing')
    sudo('ufw allow 22')
    sudo('ufw allow 80')
    sudo('ufw allow 443')
    sudo('yes | ufw enable')


def setup_docker():
    """Setup Docker engine on the remote server"""
    sudo('apt-get remove -y docker docker-engine docker.io', warn_only=True)
    sudo('apt-get install -y apt-transport-https ca-certificates curl'
         ' software-properties-common')
    sudo('curl -fsSL https://get.docker.com -o get-docker.sh')
    sudo('sh get-docker.sh')


def setup_docker_compose():
    """Setup docker-compose on the remote server"""
    sudo('curl -L "https://github.com/docker/compose/releases/download/'
         '1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o '
         '/usr/local/bin/docker-compose')
    sudo('chmod +x /usr/local/bin/docker-compose')


def docker_login():
    login_prompts = {
        'Username: ': CI_REGISTRY_USER,
        'Password: ': CI_JOB_TOKEN,
    }

    with settings(prompts=login_prompts):
        sudo(f'docker logout {CI_REGISTRY}')
        with quiet():
            sudo(f'docker login {CI_REGISTRY}')


def docker_deploy():
    require('ci_env_vars')

    sudo(f'docker image pull {CI_REGISTRY_IMAGE}:{CI_COMMIT_REF_NAME}')

    # Clean up old docker images after pulling the latest one
    sudo(f'yes | docker image prune')

    with quiet():
        with cd(env.app_dir):
            with shell_env(**env.ci_env_vars):
                sudo('docker-compose down')
                sudo('docker-compose up -d')


# Main tasks used for CI/CD automation
def setup():
    """Main setup task for running all setup tasks on the remote server"""
    if not files.exists(env.app_dir):
        upgrade()
        setup_swap()
        setup_ufw()
        setup_docker()
        setup_docker_compose()
        sudo(f'mkdir -p {env.app_dir}')
        update_files()


def pull_db():
    """
    Pulls a copy of the remote database into a local SQL file
    """
    require('ci_env_vars')
    database_prompt = {
        'Password for user root: ': env.ci_env_vars['APP_DB_PASS'],
    }

    cmd = f'docker exec -it "postgres" pg_dump --column-inserts -U odoo odoo14'
    filestoretar = f'filestore.tar.gz'
    fscmd = f'docker exec -it "odoo" bash -c "cd /tmp && tar -czf {filestoretar} -C /var/lib/odoo/filestore ."'
    fscp = f'docker cp "odoo":/tmp/{filestoretar} {filestoretar}'
    filename = f'ymc-odoo.sql'

    with settings(prompts=database_prompt):
        with cd(env.app_dir):
            sudo(f'{cmd} > {filename}')
            get(f'{filename}', f'{filename}', use_sudo=True)
            sudo(f'{fscmd}')
            sudo(f'{fscp}')
            get(f'{filestoretar}', f'{filestoretar}', use_sudo=True)


def update_local_db():
    """
    Update the local database from the SQL database file copy pulled from
    remote server by the pull_db function
    """
    with settings(warn_only=True):
        local('docker-compose -f docker-compose-dev.yml down')

    local('docker-compose -f docker-compose-dev.yml up -d db')

    with settings(warn_only=True):
        local('docker exec -it "postgres-ymc" psql -U odoo postgres -c '
              '"drop database odoo13"')

    local('docker exec -it "postgres-ymc" psql -U odoo postgres -c '
          '"create database odoo14"')
    local('docker cp ymc-odoo.sql postgres-ymc:/ymc-odoo.sql')
    local('docker exec -it "postgres-ymc" sh -c "psql -U odoo odoo14 < '
          '/ymc-odoo.sql"')
    local('docker-compose -f docker-compose-dev.yml up -d web')
    local('docker cp filestore.tar.gz odoo-ymc:/filestore.tar.gz')
    local('docker exec -it "odoo-ymc" bash -c "mkdir /var/lib/odoo/filestore"')
    local('docker exec -it "odoo-ymc" bash -c "rm -rf '
          '/var/lib/odoo/filestore/* && tar xzf filestore.tar.gz -C '
          '/var/lib/odoo/filestore"')
    local('docker container stop odoo-ymc')


def pull_db_and_update_local_dev():
    """
    Pulls a copy of the remote database into a local SQL file and update the
    local database in the postgres docker container
    """
    pull_db()
    update_local_db()


def deploy_to_local_dev():
    """
    Local development deploy task - will deploy any code changes from the local
    machine to the local docker services (started by docker-compose-dev.yml)
    """
    local('docker-compose -f docker-compose-dev.yml build web')
    local('docker container stop odoo-ymc')
    local('yes | docker container prune')
    local('docker-compose -f docker-compose-dev.yml up -d --no-deps web')


def deploy():
    """
    Main deploy task for updating code changes to the remote server.
    Will also set up the server if it wasn't done already
    """
    if env.deploy_env == 'localdev':
        deploy_to_local_dev()
    else:
        setup()
        update_files()
        docker_login()
        docker_deploy()
