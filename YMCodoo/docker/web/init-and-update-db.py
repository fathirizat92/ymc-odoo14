#!/usr/bin/env python3
"""
This module configures odoo settings for this project.

Note: This function is called everytime the Docker image is restarted as it is
invoked from the entrypoint script (entrypoint.sh)
"""

import os
import sys
import logging
import xmlrpc.client
import erppeek
import requests
# import base64


def setup_logger():
    logger = logging.getLogger('init-and-update-db')

    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s %(name)s: '
                                  '%(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


logger = setup_logger()

# Reporting URL System Parameters
# Additional System Parameters
# Use system parameters for VPN database query


def setup_sys_parameters(models, db, uid, password):
    param_list = [
        ['config.ver', '0.1'],
        ['report.url', 'http://127.0.0.1:8069'],
        ['web.base.url.freeze', 'true']
    ]

    if os.getenv('APP_SSL_DOMAIN'):
        param_list.append(['web.base.url',
                           'https://%s' % os.getenv('APP_SSL_DOMAIN')])

    for param in param_list:
        param_key, param_value = param

        param_result = models.execute_kw(db, uid, password,
                                         'ir.config_parameter', 'search_read',
                                         [[('key', '=', param_key)]])
        if (len(param_result) == 0):
            param_id = models.execute_kw(
                db, uid, password,
                'ir.config_parameter', 'create', [{
                    'key': param_key,
                    'value': param_value
                }])
            logger.info(f'[{param_id}] {str(param_key)}={param_value}'
                        ' [inserted]')
        else:
            if (param_key == 'config.ver' and param_value
                    <= param_result[0]['value']):
                logger.info('config.ver below version installed.')

            if (param_value != param_result[0]['value']):
                models.execute_kw(db, uid, password,
                                  'ir.config_parameter',
                                  'write',
                                  [[param_result[0]
                                    ['id']], {
                                        'value': param_value
                                    }])
                logger.info(f'{param_key}={param_value} [updated]')
            else:
                logger.info(f'{param_key} update [skip]')

# List of Modules


def install_modules(models, db, uid, password):
    module_list = [
        ['Invoicing', 'account'],
        ['Sales', 'sale_management'],
        ['Account Bank Statement Import', 'account_bank_statement_import'],
        ['Import Bills/Invoices From XML', 'account_facturx'],
        ['Analytic Accounting', 'analytic'],
        ['Signup', 'auth_signup'],
        ['Base', 'base'],
        ['Base import', 'base_import'],
        ['Initial Setup Tools', 'base_setup'],
        ['IM Bus', 'bus'],
        ['Calendar - SMS', 'calendar_sms'],
        ['Contacts', 'contacts'],
        ['SMS in CRM', 'crm_sms'],
        ['KPI Digests', 'digest'],
        ['Email Gateway', 'fetchmail'],
        ['In-App Purchases', 'iap'],
        ['Generic - Accounting', 'l10n_generic_coa'],
        ['Discuss', 'mail'],
        ['OdooBot', 'mail_bot'],
        ['Partner Autocomplete', 'partner_autocomplete'],
        ['Payment Acquirer', 'payment'],
        ['Transfer Payment Acquirer', 'payment_transfer'],
        ['Products & Pricelists', 'product'],
        ['Resource', 'resource'],
        ['Sales', 'sale'],
        ['Opportunity to Quotation', 'sale_crm'],
        ['Sales Teams', 'sales_team'],
        ['Inventory', 'stock'],
        ['SMS gateway', 'sms'],
        ['Snail Mail', 'snailmail'],
        ['snailmail_account', 'snailmail_account'],
        ['Units of measure', 'uom'],
        ['UTM Trackers', 'utm'],
        ['Web', 'web'],
        ['Odoo Web Diagram', 'web_diagram'],
        ['Web Editor', 'web_editor'],
        ['Gauge Widget for Kanban', 'web_kanban_gauge'],
        ['Tours', 'web_tour'],
        ['Unsplash Image Library', 'web_unsplash'],
        ['Calendar', 'calendar'],
        ['Customer Portal', 'portal'],
        ['Web Routing', 'http_routing'],
        ['Phone Numbers Validation', 'phone_validation'],
        ['Purchase', 'purchase'],
        ['Manufacturing', 'mrp'],
        ['Purchase Stock', 'purchase_stock'],
        ['Purchase Agreements', 'purchase_requisition'],
        ['Purchase Requisition Stock', 'purchase_requisition_stock'],
        ['Products Expiration Date', 'product_expiry'],
        ['YMC Module', 'ymc-odoo']
    ]
    for module_info in module_list:
        module_name, module_code = module_info

        # Get the module's details
        result = models.execute_kw(
            db, uid, password,
            'ir.module.module', 'search_read', [[('name', '=', module_code)]],
        )
        # Check if module is uninstalled, install it
        if len(result) > 0 and result[0]['state'] == 'uninstalled':
            logger.info(f'Install `{module_name}` Module')
            module_id = models.execute_kw(
                db, uid, password,
                'ir.module.module', 'search', [[('name', '=', module_code)]],
            )
            models.execute_kw(
                db, uid, password,
                'ir.module.module', 'button_immediate_install', [module_id],
            )
        else:
            if (module_code == 'ymc-odoo'):
                module_id = models.execute_kw(
                    db, uid, password,
                    'ir.module.module', 'search',
                    [[('name', '=', module_code)]],
                )
                models.execute_kw(
                    db, uid, password,
                    'ir.module.module', 'button_immediate_upgrade',
                    [module_id],
                )
            else:
                logger.info(f'Module `{module_name}` is installed already')

# Adding SMTP configuration to the Odoo


def setup_smtp(models, db, uid, password):
    logger.info('Configuring SMTP settings ...')
    smtp_param = {
        'name': 'Default SMTP',
        'smtp_host': os.getenv('APP_SMTP_HOST'),
        'smtp_port': os.getenv('APP_SMTP_PORT'),
        'smtp_encryption': os.getenv('APP_SMTP_ENCRYPTION'),
        'smtp_user': os.getenv('APP_SMTP_USER'),
        'smtp_pass': os.getenv('APP_SMTP_PASS')
    }
    smtp_result = models.execute_kw(db, uid, password, 'ir.mail_server',
                                    'search_read',
                                    [[('name', '=', 'Default SMTP')]])
    if len(smtp_result) > 0:
        smtp_id = smtp_result[0]['id']
        smtp_update_result = models.execute_kw(db, uid, password,
                                               'ir.mail_server',
                                               'write',
                                               [[smtp_id], smtp_param])
        logger.info(f'SMTP [updated] {smtp_update_result}')
    else:
        smtp_update_result = models.execute_kw(db, uid, password,
                                               'ir.mail_server', 'create',
                                               [smtp_param])
        logger.info(f'SMTP [created] {smtp_update_result}')

# Default Company Settings


def setup_company(models, db, uid, password):
    logger.info('Setting up company details ...')

    # Get country id
    result = models.execute_kw(
        db, uid, password,
        'res.country', 'search', [[['name', '=', 'Malaysia']]],
    )
    country_id = result[0]
    logger.info('Country ID: ' + str(country_id))

    # Get state id
    result = models.execute_kw(
        db, uid, password,
        'res.country.state', 'search', [[['name', '=', 'Kuala Lumpur']]]
    )
    if (len(result) == 0):
        result = models.execute_kw(
            db, uid, password,
            'res.country.state', 'search',
            [[['name', '=', 'Wilayah Persekutuan']]]
        )
    state_id = result[0]
    logger.info('State ID: ' + str(state_id))

    # Update company details
    models.execute_kw(db, uid, password, 'res.company', 'write', [[1], {
        'name': 'YMC',
        'street': '',
        'street2': '',
        'city': '',
        'state_id': state_id,
        'zip': '',
        'country_id': country_id,
        'email': '',
    }])

    # Currency
    # Get currency id, activate it if necessary
    ccy_id = None
    result = models.execute_kw(
        db, uid, password,
        'res.currency', 'search', [[['name', '=', 'MYR']]],
    )
    if len(result) == 0:  # unable to find the currency
        # Find from all inactive currencies
        result = models.execute_kw(
            db, uid, password,
            'res.currency', 'search',
            [[['active', '=', False], ['name', '=', 'MYR']]]
        )

        if len(result) > 0:
            ccy_id = result[0]
            # Set the currency as active
            models.execute_kw(
                db, uid, password,
                'res.currency', 'write',
                [[ccy_id], {
                    'active': True
                }],
            )
    else:
        ccy_id = result[0]
    logger.info('Currency ID: ' + str(ccy_id))

    try:
        update_ccy_status = models.execute_kw(db, uid, password,
                                              'res.company', 'write', [[1], {
                                                'currency_id': ccy_id
                                              }])
        logger.info("Currency Updated: "+str(update_ccy_status))
    except Exception:
        logger.info("Fault 2: 'You cannot change the currency of the "
                    "company since some journal items already exist'")

    # Company Logo
    # try:
    #     image = open("./logo.png", "rb")
    #     BinaryImage = xmlrpc.client.Binary(image.read())
    #     BytesImage = BinaryImage.data
    #     ImageBase64 = base64.b64encode(BytesImage)
    #     ImageSend = ImageBase64.decode('ascii')
    #     logo_upload_status = models.execute_kw(db, uid, password,
    #                                            'res.company',
    #                                            'write', [[1], {
    #                                             'logo': ImageSend
    #                                             }])
    #     logger.info("Upload Company Logo: "+str(logo_upload_status))
    # except Exception:
    #     logger.info('Company Logo Not Found')


# ERP-20 : Enable multi currency
def setup_multi_currency(models, db, uid, password):
    currency_ids = {'MYR': None,
                    'SGD': None,
                    'USD': None}

    for cy in currency_ids.keys():
        result = models.execute_kw(
            db, uid, password,
            'res.currency', 'search', [[['name', '=', cy]]],
        )
        if len(result) == 0:  # unable to find the currency
            # Find from all inactive currencies
            result = models.execute_kw(
                db, uid, password,
                'res.currency', 'search',
                [[['active', '=', False], ['name', '=', cy]]]
            )

            if len(result) > 0:
                currency_ids[cy] = result[0]
                # Set the currency as active
                models.execute_kw(
                    db, uid, password,
                    'res.currency', 'write',
                    [[currency_ids[cy]], {
                        'active': True
                    }],
                )
        else:
            currency_ids[cy] = result[0]

    reconf = models.execute_kw(db, uid, password, 'res.config.settings',
                               'create', [{
                                'group_multi_currency': True,
                                'currency_id': currency_ids['MYR'],
                                }])
    if reconf:
        models.execute_kw(db, uid, password, 'res.config.settings', 'execute',
                          [reconf])


# ERP-6: Enabling discounts as part of new fields
def setup_discount_in_settings(models, db, uid, password):
    reconf = models.execute_kw(db, uid, password, 'res.config.settings',
                               'create', [{
                                'group_discount_per_so_line': True,
                                }])
    if reconf:
        models.execute_kw(db, uid, password, 'res.config.settings', 'execute',
                          [reconf])


# ERP-25: Enabling PO approval
def setup_po_in_settings(models, db, uid, password):
    reconf = models.execute_kw(db, uid, password, 'res.config.settings',
                               'create', [{
                                'po_order_approval': True,
                                'po_double_validation_amount': 0.0
                                }])
    if reconf:
        models.execute_kw(db, uid, password, 'res.config.settings', 'execute',
                          [reconf])


def configure_odoo(db, url, username, password):
    # endpoint: [xmlrpc/2/common] login with `authenticate` function
    common = xmlrpc.client.ServerProxy(f'{url}/xmlrpc/2/common')
    uid = common.authenticate(db, username, password, {})

    # endpoint: [xmlrpc/2/object] use to call odoo methods of odoo models
    # via `execute_kw` function
    models = xmlrpc.client.ServerProxy(f'{url}/xmlrpc/2/object')

    setup_sys_parameters(models, db, uid, password)

    install_modules(models, db, uid, password)

    # setup_smtp(models, db, uid, password)

    setup_company(models, db, uid, password)

    setup_multi_currency(models, db, uid, password)

    setup_discount_in_settings(models, db, uid, password)

    setup_po_in_settings(models, db, uid, password)


def main():
    DATABASE = 'odoo13'
    SERVER = 'http://web:8069'
    ADMIN_USERNAME = os.getenv('APP_ODOO_ADMIN_USER')
    ADMIN_PASSWORD = os.getenv('APP_ODOO_ADMIN_PASS')

    client = erppeek.Client(server=SERVER)

    if DATABASE not in client.db.list():
        logger.info('Database does not exist yet, creating one ...')

        # We could use erppeek to create the database like this:
        # client.create_database(ADMIN_PASSWORD, DATABASE)
        # but for some reason it does not work properly, sometimes resulting in
        # an 'Access denied' error.

        # Instead, we'll use the requests to programatically POST the database
        # creation form on the web UI to create the initial database
        payload = {
            'name': DATABASE,
            'login': ADMIN_USERNAME,
            'password': ADMIN_PASSWORD,
            'master_pwd': 'admin',
            'lang': 'en_US',
            'phone': '',
            'country_code': '',
        }
        response = requests.post(SERVER + '/web/database/create', data=payload)
        if response.status_code == 200:
            logger.info('Database created!')
        else:
            logger.info('Failed to create database. Exitting.')
            return
    else:
        logger.info(f'Database {DATABASE} already exists.')

    configure_odoo(DATABASE, SERVER, ADMIN_USERNAME, ADMIN_PASSWORD)


if __name__ == '__main__':
    main()
